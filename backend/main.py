import asyncio
import logging
import os
import weakref
from abc import ABCMeta
from json.decoder import JSONDecodeError
from typing import Union

import aioredis
from aiohttp import ClientSession, WSCloseCode
from aiohttp.web import run_app, middleware, HTTPUnauthorized, HTTPForbidden, HTTPBadRequest, Response, json_response, \
    Request, Application
from aiohttp_session import Session, get_session, session_middleware
from aiohttp_session.redis_storage import RedisStorage

from adm.database import db_session, User
from backend.game_session_workers import master_channel_listener
from backend.handlers.game_session import GameSessionHandler, GameSessionQRHandler, ChooseBuildHandler, \
    StartGameHandler, StepHandler
from backend.handlers.search import GeoSearchHandler
from backend.handlers.social_auth import SocialAuthHandler, SocialAuthCompleteHandler
from backend.handlers.user_info import UserInfoHandler
from backend.handlers.websocket import WebSocketHandler

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')

WS_SERVER_UNIX_SOCKET_PATH: str = os.getenv('WS_SERVER_UNIX_SOCKET_PATH')


@middleware
async def ws_request_middleware(request: Request, handler: ABCMeta) -> Union[Response, json_response]:
    session: Session = await get_session(request)
    user_id: str = session.get('id')
    request.app.current_user_id: str = user_id
    resp: Union[Response, json_response] = await handler(request)
    return resp


@middleware
async def reject_unauth_request(request: Request, handler: ABCMeta) -> Union[Response, json_response]:
    auth_url: str = request.app.router['auth'].url_for().raw_path
    auth_complete_url: str = request.app.router['authcomplete'].url_for().raw_path

    session: Session = await get_session(request)
    user_id: str = session.get('id')

    # Если клиент не авторизован с помощью secure cookie, вернем ему 401
    if not user_id and request.url.raw_path not in (auth_url, auth_complete_url):
        # TODO: Возвращать полный URL с доменом ?
        # TODO: 401 не предполагает редиректа и location в нем - нарушение протокола
        raise HTTPUnauthorized(headers={'Location': auth_url})

    # Если авторизованный пользователь отсутствует в БД: вернем ему запрет доступа и удалим куку.
    # Это условие никогда не должно выполняться если приложение работает корректно.
    if request.url.raw_path not in (auth_url, auth_complete_url):
        user_id = session.get('id')
        if not request.app['db_session'].query(User).filter_by(id=user_id).one_or_none():
            session.clear()
            raise HTTPForbidden(text='Авторизованный пользователь не найден в БД.')

    if request.headers.get('content-type') == 'application/json':
        try:
            await request.json()
        except JSONDecodeError:
            log.exception(f'Can`t parse body as JSON: ')
            raise HTTPBadRequest(text='Use JSON as data transport, please.')

    request.app.current_user_id = user_id

    resp: Union[Response, json_response] = await handler(request)
    return resp


async def start_master_redis_listener(app):
    app['redis_listener'] = app.loop.create_task(master_channel_listener(app))


async def clean_ws(app):
    for ws in set(app['websockets']):
        await ws.close(code=WSCloseCode.GOING_AWAY,
                       message='Server shutdown')
    log.debug('All web sockets are gracefully closed')


async def stop_redis_listeners(app):
    app['redis_listener'].cancel()
    await app['redis_listener']
    log.debug('Master redis listener stopped')
    for listener in app['game_session_listeners']:
        listener.cancel()
        await listener
        log.debug('Game session redis listener stopped')


async def close_redis_writer(app):
    keys = await app['redis_writer'].keys('*')
    log.debug(f'Redis writer keys are: {keys}')

    # Can`t perform action like that:
    # await app['redis'].delete(*keys)
    for key in keys:
        await app['redis'].delete(key)
    app['redis_writer'].close()
    log.debug('Redis writer is closed')


async def http_app_factory():
    """
    Этот инстанс обслуживает http хэндлеры, работаетс  БД, шлет сообщения ws_app через redis.
    """
    loop = asyncio.get_event_loop()

    redis_pool = await aioredis.create_redis_pool(
        ('redis', 6379),
        db=0,
    )

    app: Application = Application(loop=loop, middlewares=[
        session_middleware(RedisStorage(redis_pool)),
        reject_unauth_request,
    ])

    app['db_session'] = db_session
    app['redis_writer'] = redis_pool
    app['http_client_session']: ClientSession = ClientSession(raise_for_status=False, read_timeout=60, conn_timeout=60)

    app.router.add_route('GET', '/auth', SocialAuthHandler, name='auth')
    app.router.add_route('GET', '/authcomplete', SocialAuthCompleteHandler, name='authcomplete')

    app.router.add_route('POST', '/game_session', GameSessionHandler, name='create_game_session')
    app.router.add_route('GET', '/game_session', GameSessionHandler, name='join_game_session')
    app.router.add_route('GET', '/game_session_qr', GameSessionQRHandler, name='get_game_session_qr')
    app.router.add_route('POST', '/choose_build', ChooseBuildHandler, name='choose_build')
    app.router.add_route('POST', '/start_game', StartGameHandler, name='start_game')
    app.router.add_route('POST', '/make_step', StepHandler, name='make_step')

    app.router.add_route('GET', '/geo_search', GeoSearchHandler, name='geo_search')
    app.router.add_route('PATCH', '/user_info', UserInfoHandler, name='update_user_info')
    app.router.add_route('POST', '/user_email', UserInfoHandler, name='update_user_email')

    app.on_cleanup.append(lambda application: app['http_client_session'].close())
    app.on_shutdown.append(close_redis_writer)
    return app


async def ws_app_factory():
    """
    Этот инстанс работает с ws клиентами и dict'ом игровых данных. Слушает сообщения от http-app в redis корутинах.
    """
    loop = asyncio.get_event_loop()

    redis_pool = await aioredis.create_redis_pool(
        ('redis', 6379),
        db=0,
    )

    app: Application = Application(loop=loop, middlewares=[
        session_middleware(RedisStorage(redis_pool)),
        ws_request_middleware,
    ])

    app['websockets'] = weakref.WeakSet()
    app['game_session_listeners'] = weakref.WeakSet()
    app['gameflow_data'] = dict()
    app['redis_listener'] = asyncio.ensure_future(master_channel_listener(app))

    app['redis_writer'] = redis_pool
    app.router.add_route('GET', '/game_session_qr', GameSessionQRHandler, name='get_game_session_qr')
    app.router.add_route('GET', '/ws', WebSocketHandler, name='ws_game_session')

    app.on_shutdown.append(clean_ws)
    app.on_shutdown.append(stop_redis_listeners)
    app.on_shutdown.append(close_redis_writer)
    return app


def main():
    app = ws_app_factory()
    run_app(app, path=WS_SERVER_UNIX_SOCKET_PATH)
    log.info('Shutdown ws app.')


if __name__ == '__main__':
    main()
