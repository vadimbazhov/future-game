from aiohttp.web import View, json_response
from sqlalchemy import bindparam, func
from sqlalchemy.ext import baked

from adm.database import City, Region


class GeoSearchHandler(View):
    @staticmethod
    def is_ascii(pattern):
        try:
            pattern.encode('ascii')
            return False
        except UnicodeEncodeError:
            return True

    @staticmethod
    def get_results(db_session, entity, query, is_unicode, region_id):
        bakery = baked.bakery()
        baked_query = bakery(lambda session: db_session.query(entity))
        query = query.lower()

        if region_id:
            baked_query += lambda q: q.filter(entity.region_id == bindparam('region_id'))

        if is_unicode:
            baked_query += lambda q: q.filter(func.lower(entity.title_ru).startswith(query))

            return sorted([
                (x.title_ru, x.id) for x in baked_query(db_session).params(region_id=region_id).all()
            ])[:10]
        else:
            altnames_baked_query = baked_query
            altnames_baked_query += lambda q: q.filter(func.lower(entity.alt_title).startswith(query))

            baked_query += lambda q: q.filter(func.lower(entity.title).startswith(query))

            titles = [
                (x.title, x.id) for x in baked_query(db_session).params(region_id=region_id).all()
            ]

            alt_titles = [
                (x.alt_title, x.id) for x in
                altnames_baked_query(db_session).params(region_id=region_id).all()
            ]

            return sorted([x for x in set(titles) | set(alt_titles)])[:10]

    async def get(self) -> json_response:
        """
        Поиск по странам, регионам, городам для формы создания игровой сессии.
        Принимает искомые строки по этим сущностям в URL-параметрах GET-запроса и ищет по соотв. сущностям в БД.
        """
        min_query_length = 1

        response_url = self.request.rel_url

        regions_query = response_url.query.get('regions')
        cities_query = response_url.query.get('cities')

        region_id = response_url.query.get('region_id')

        regions = []
        cities = []

        db_session = self.request.app['db_session']

        if regions_query and len(regions_query) > min_query_length:
            is_unicode = self.is_ascii(regions_query)
            regions = self.get_results(db_session, Region, regions_query, is_unicode, region_id)

        if cities_query and len(cities_query) > min_query_length:
            is_unicode = self.is_ascii(cities_query)
            cities = self.get_results(db_session, City, cities_query, is_unicode, region_id)

        return json_response({
            'regions': regions,
            'cities': cities,
        })
