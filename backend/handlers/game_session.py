import logging
from secrets import choice
from tempfile import NamedTemporaryFile
from typing import Union

import qrcode
from aiohttp.web import View, json_response, HTTPNotFound, HTTPInternalServerError, HTTPBadRequest, HTTPException, \
    HTTPForbidden, StreamResponse

from adm.database import GameSession, UserSession, Build, User, Skill, Crisis, Quest, Step, GeoData

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')


def gen_pin():
    pin = ''
    for i in range(0, 6):
        rndint = choice(range(0, 10))
        if i == 0:
            while rndint == 0:
                rndint = choice(range(0, 10))
        pin += (str(rndint))
    return int(pin)


def get_builds(db_session):
    """Model.__dict__ передает еще и sequience, нам это не надо. Полей мало, формирую dict вручную."""
    builds = []
    for build in db_session.query(Build).all():
        builds.append({
            'id': build.id,
            'description': build.description,
            'title': build.title,
            'image': build.image,
        })
    return builds


class GameSessionHandler(View):
    async def post(self) -> Union[json_response, HTTPException]:
        """
            Создание игры (игровой сессии) преподавателем, заполняя анкету с данными.
            Возвращаем:
                - ID игровой сессии
                - URL доступа к экрану входа в игровую сессию, где необходимо ввести ID сессии
                - Ссылку на генерацию QR-кода ссылки для этой игровой сессии
                - Ссылку на WS игровой сессии
        """

        author_id = self.request.app.current_user_id
        data = await self.request.json()
        db_session = self.request.app['db_session']

        region = data.get('region')
        city = data.get('city')
        school = data.get('school')
        grade = data.get('grade')
        residency = data.get('residency')
        max_players = data.get('max_players')

        if not all((region, city, school, grade, residency, max_players)):
            raise HTTPBadRequest(text='Insufficient incoming data.')

        # TODO: обрабатывать ошибки, какие именно эксепшны ?
        for i in range(10):
            # Рандомное 6-знаковое число
            pin = gen_pin()
            if not db_session.query(GameSession).filter_by(pin=pin).one_or_none():
                game_session = GameSession(
                    pin=pin,
                    max_players=max_players,
                    author_id=author_id,
                )
                break

        if not game_session:
            raise HTTPInternalServerError(text='Unable to generate unique hash for Game Session.')

        geo_data = GeoData(
            region=region,
            city=city,
            school=school,
            grade=grade,
            residency=residency,
            game_session=game_session,
        )

        db_session.add(game_session, geo_data)
        db_session.commit()

        author = db_session.query(User).filter_by(id=author_id).one_or_none()

        builds = get_builds(self.request.app['db_session'])

        await self.request.app['redis_writer'].publish_json('master', {
            'action': 'create',
            'pin': pin,
            'author_id': author_id,
            'author_name': f'{author.first_name} {author.last_name}',
            'max_players': max_players,
            'grade': geo_data.grade,
            'builds': builds,
        })

        join_game_url = self.request.app.router['join_game_session'].url_for().raw_path
        join_game_url = f'/api/{join_game_url}?pin={pin}'

        get_qr_code_url = self.request.app.router['get_game_session_qr'].url_for().raw_path
        get_qr_code_url = f'/api/{get_qr_code_url}?pin={pin}'

        ws_url = f'/api/ws?pin={pin}'

        return json_response({
            'game_session_pin': game_session.pin,
            'qr_code_url': get_qr_code_url,
            'join_game_url': join_game_url,
            'ws_url': ws_url,
        })

    async def get(self):
        """
            Запрос игровой сессии учеником. С вводом PIN сессии.
            Возвращаем ссылку на генератор QR-кода с сылкой на web socket
        """
        db_session = self.request.app['db_session']
        pin = int(self.request.query.get('pin'))

        if not db_session.query(GameSession).filter_by(pin=pin).one_or_none():
            raise HTTPNotFound(text=f'Игровая сессия с запрошенным PIN: {pin} не найдена.')

        return json_response({
            "game_session_link": f'/api/ws?pin={pin}',
            "game_session_pin": pin,
        })


class GameSessionQRHandler(View):
    async def get(self) -> Union[StreamResponse, HTTPException]:
        """
            Ссылку на вебсокет для конкретной игровой сессии конвертирует в QR-код
            и отдает его в виде картинки на клиент.
        """
        pin = int(self.request.query.get('pin'))
        if not pin:
            raise HTTPBadRequest(text='Не был передан ID сессии.')

        if not self.request.app['db_session'].query(GameSession).filter_by(pin=pin).one_or_none():
            raise HTTPNotFound(text=f'Игровая сессия с запрошенным PIN: {pin} не найдена.')

        ws_url = f'/ws?game_session_id={pin}'
        log.debug(ws_url)

        qr_file = NamedTemporaryFile()

        img = qrcode.make(ws_url).get_image()
        img.save(qr_file.name, 'JPEG', quality=50)

        resp = StreamResponse(headers={'Content-Type': 'image/jpeg'})
        await resp.prepare(self.request)
        with open(qr_file.name, 'rb') as file:
            await resp.write(file.read())

        return resp


class ChooseBuildHandler(View):
    async def post(self) -> Union[json_response, HTTPException]:
        """
            Игрок подключен к сокету и выбирает себе персонажа. Перед этим доступные персонажи отдаются ему в сокет.
        """
        player_id = self.request.app.current_user_id

        data = await self.request.json()
        pin = data.get('pin')
        build_id = data.get('build_id')

        db_session = self.request.app['db_session']
        game_session = db_session.query(GameSession).filter_by(pin=pin).one_or_none()
        if not game_session:
            raise HTTPNotFound(text=f'Игровая сессия с запрошенным PIN: {pin} не найдена.')

        build = db_session.query(Build).filter_by(id=build_id).one_or_none()

        user_session = UserSession(game_session=game_session, user_id=player_id, build_id=build_id)
        db_session.add(user_session)
        db_session.commit()

        initial_skills = {}
        for skill in db_session.query(Skill).all():
            if skill in build.skills:
                initial_skills[skill.id] = 3
            else:
                initial_skills[skill.id] = 1

        pub = self.request.app['redis_writer']
        await pub.publish_json(f'session-{pin}', {
            'action': 'choose_build',
            'player_id': player_id,
            'payload': {
                'user_session_id': user_session.id,
                'initial_build': build.title,
                'initial_skills': initial_skills,
                'actual_skills': initial_skills,
            }
        })

        return json_response({'success': True})


class StartGameHandler(View):
    async def post(self) -> Union[json_response, HTTPException]:
        """
            Игроки открыли сокеты и ожидают там (макет: ...будущее загружается...)
            Преподаватель (автор игровой сессии) начинает игру по кнопке "Начать игру", когда сочтет нужным.
            В сокет всем игрокам надо отдать инфу по персонажам/характерам доступным к выбору.
        """
        log.debug('In game start handler')

        teacher_id = self.request.app.current_user_id

        data = await self.request.json()
        pin = data.get('pin')

        db_session = self.request.app['db_session']

        game_session = db_session.query(GameSession).filter_by(pin=pin, author_id=teacher_id).one_or_none()
        if not game_session:
            raise HTTPNotFound(text=f'Игровая сессия с PIN: {pin} автором которой являетесь вы, не найдена.')

        crisises = {}
        for crisis in db_session.query(Crisis).all():
            crisises[crisis.id] = {
                'title': crisis.title,
                'description': crisis.description,
                'image': crisis.image,
                'solved': False,
                'quests': {}
            }
            for quest in db_session.query(Quest).filter(Quest.crisis == crisis).all():
                crisises[crisis.id]['quests'][quest.id] = {
                    'description': quest.description,
                    'title': quest.description,
                    'image': quest.image,
                    'weaknesses': [x.id for x in quest.weaknesses],
                    'hitpoints': 0,
                }

        await self.request.app['redis_writer'].publish_json(f'session-{pin}', {
            'action': 'start_game',
            'crisises': crisises,
        })

        return json_response({'success': True})


def do_math(game_session_dict, quest_weaknesses, player_id, crisis_id, quest_id):
    """
        Просчет математики по результатам хода игрока (удар по выбранному Quest)
        TODO: Актуализировать математику с уточнением ТЗ в будущем
        TODO: Вероятно надо тут подсчитывать побежден quest или нет, и кризис. Прописывая это в дикт.
    """
    actual_player_skills = game_session_dict['player_stats'][player_id]['actual_skills'].copy()
    current_quest_damage = game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints']

    log.debug(f'Actual player skills: {actual_player_skills}')
    log.debug(f'Quest weaknesses (skill ids): {quest_weaknesses}')

    # сопоставим слабости квеста с навыками игрока
    for weakness in quest_weaknesses:
        # добавим damage'й квесту согласно текущему значению скилла игрока, который релевантен слабости квеста
        current_quest_damage += actual_player_skills[weakness]

        # удар релевантный скиллу игрока
        # прокачаем релевантный скилл игрока на 1 очко
        if actual_player_skills[weakness] > 1:
            log.debug('Hit')
            actual_player_skills[weakness] += 1
        # нерелевантный удар
        # снимем с прокачаных скиллов игрока по 1 очку
        # но прокачаем релевантный скилл игрока на 1 очко
        else:
            log.debug('Miss')
            for skill, value in actual_player_skills.items():
                if value > 1:
                    actual_player_skills[skill] -= 1
            actual_player_skills[weakness] += 1

        # актуализируем глобальный дикт
        game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints'] = current_quest_damage
        game_session_dict['player_stats'][player_id]['actual_skills'] = actual_player_skills

        log.debug(f"Now players skills are: {game_session_dict['player_stats'][player_id]['actual_skills']}")
        log.debug(f"Now quest damage is: {game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints']}")


class StepHandler(View):
    async def post(self) -> Union[json_response, HTTPException]:
        """
            Игрок выбирает Quest по которому хочет нанести удар.
            Мы должны:
            - записать его выбор (ход) в БД
            - просчитать урон нанесенный Quest'у и обновить его в глобальном дикте
            - просчитать изменение навыков у игрока, обновить их в кеше
        """
        log.debug('In Step handler')

        player_id = self.request.app.current_user_id
        data = await self.request.json()
        pin = data.get('pin')
        crisis_id = data.get('crisis_id')
        quest_id = data.get('quest_id')

        db_session = self.request.app['db_session']
        game_session = db_session.query(GameSession).filter_by(pin=pin).one_or_none()
        if not game_session:
            raise HTTPNotFound(text=f'Игровая сессия с запрошенным PIN: {pin} не найдена.')

        user_session = db_session.query(UserSession).filter_by(game_session=game_session,
                                                               user_id=player_id).one_or_none()
        if not user_session:
            raise HTTPForbidden(text=f'Игрок {player_id} не зарегистрирован в игре: {pin}.')

        # TODO: может еще проверять соответствуют ли заявленные Quest и Crisis между собой? Мне кажется - избыточно.

        step = Step(user_session=user_session, crisis_id=crisis_id, quest_id=quest_id)
        db_session.add(step)
        db_session.commit()

        quest = db_session.query(Quest).filter_by(id=quest_id).one_or_none()
        quest_weaknesses = [w.id for w in quest.weaknesses]

        # Некоторые ID к сожалению конвертим в str так как ID диктов на которые они указывают при конвертации в JSON
        # так же становятся строковыми.
        await self.request.app['redis_writer'].publish_json(f'session-{pin}', {
            'action': 'make_step',
            'quest_weaknesses': quest_weaknesses,
            'player_id': player_id,
            'crisis_id': str(crisis_id),
            'quest_id': str(quest_id),
        })

        return json_response({'success': True})
