import logging
import os
from typing import Union

from aiohttp.web import View, json_response, HTTPFound, HTTPBadRequest, HTTPForbidden, HTTPServiceUnavailable, \
    HTTPException
from aiohttp_session import Session, get_session
from yarl import URL

from adm.database import User

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')

VK_APP_ID: str = os.getenv('VK_APP_ID')
VK_APP_KEY: str = os.getenv('VK_APP_KEY')
DOMAIN: str = os.getenv('DOMAIN')
REDIRECT_URI: str = f'http://{DOMAIN}/api/authcomplete'
API_V: str = '5.80'


class SocialAuthHandler(View):
    async def get(self) -> Union[json_response, HTTPException]:
        # todo: позапрашивать права для direct message'ей
        scope: str = 'email,photo,offline'
        session: Session = await get_session(self.request)

        if not session.get('auth_token'):
            url: str = f'https://oauth.vk.com/authorize?' \
                       f'client_id={VK_APP_ID}&' \
                       f'display=page&' \
                       f'redirect_uri={REDIRECT_URI}&' \
                       f'scope={scope}&' \
                       f'response_type=code&' \
                       f'v={API_V}'
            raise HTTPFound(location=url)

        return json_response({'authorized': True})


class SocialAuthCompleteHandler(View):
    async def get(self) -> Union[json_response, HTTPException]:
        response_url: URL
        error: str
        error_description: str
        access_code: str
        frontend_entrypoint: str
        url: str
        json_body: dict
        access_token: str
        expires_in: str
        user_id: str
        method_name: str
        fields: str
        vk_response: Union[dict, str]
        session: Session

        response_url = self.request.rel_url

        error = response_url.query.get('error')
        # тут какой то баг с response_url.query. спровоцировать, отрефакторить.
        if error:
            error_description = response_url.query.get('error_description')
            raise HTTPForbidden(text=f'VK responded with error: {error}: {error_description}')

        access_code = response_url.query.get('code', None)
        frontend_entrypoint = response_url.query.get('code', None)
        if not access_code:
            raise HTTPBadRequest(text='No access code found in VK response.')

        log.debug('Starting an access token request to VK.')
        url = f'https://oauth.vk.com/access_token?' \
              f'client_id={VK_APP_ID}&' \
              f'client_secret={VK_APP_KEY}&' \
              f'redirect_uri={REDIRECT_URI}&' \
              f'code={access_code}'

        async with self.request.app['http_client_session'].get(url) as request:
            json_body = await request.json()

        error = json_body.get('error', None)
        if error:
            error_description = url.query.get('error_description')
            raise HTTPForbidden(text=f'VK responded with error on access token request: {error}: {error_description}')

        if not json_body:
            raise HTTPServiceUnavailable(text='VK responded with empty response on access token request')

        access_token = json_body.get('access_token')
        expires_in = json_body.get('expires_in')
        vk_user_id = json_body.get('user_id')
        vk_user_email = json_body.get('email')
        if not vk_user_email:
            raise HTTPForbidden(text=f'VK profile has no email. This profile can`t participate in the game flow.')

        # expires_in бессрочен (0) так как мы запрашиваем бессрочный access token
        if not access_token or expires_in != 0:
            raise HTTPServiceUnavailable(text='VK responded with malformed response body on access token request')

        log.debug(f'Successfully authorized via VK.')

        method_name = 'users.get'
        fields = 'photo_id,sex,bdate,has_photo,photo_200_orig'
        url = f'https://api.vk.com/method/{method_name}?' \
              f'user_ids={vk_user_id}&' \
              f'fields={fields}&' \
              f'access_token={access_token}&' \
              f'v={API_V}'

        async with self.request.app['http_client_session'].get(url) as request:
            vk_response = await request.json()
        vk_response = vk_response['response'].pop()
        vk_response['email'] = vk_user_email
        vk_response['access_token'] = access_token

        user_id = self.update_user_info(vk_response)

        session = await get_session(self.request)
        session['id'] = user_id
        session['email'] = vk_response['email']

        return HTTPFound(frontend_entrypoint)

    def update_user_info(self, vk_response):
        db_session = self.request.app['db_session']

        user = db_session.query(User).filter(User.id == vk_response['id']).one_or_none()

        if not user:
            log.error('No VK acc found, create it.')
            user = User(id=vk_response['id'])

        user.first_name = vk_response['first_name'],
        user.last_name = vk_response['last_name'],
        user.birth_date = vk_response['bdate'],
        user.email = vk_response['email'],
        user.photo = vk_response['photo_200_orig'],
        user.access_token = vk_response['access_token'],

        db_session.add(user)
        db_session.commit()
        return user.id
