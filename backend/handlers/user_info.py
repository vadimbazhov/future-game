import logging
from typing import Union

from aiohttp.web import View, json_response, HTTPBadRequest, HTTPException

from adm.database import User, GeoData

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')


class UserInfoHandler(View):
    async def patch(self) -> Union[json_response, HTTPException]:
        """
            Заполнение анкеты пользователем который не будет участвовать в игре.
            Нужно чтобы иметь возможность участвовать в олимпиаде степики не играя в игру.
            Пользователь точно имеется в БД так как к хэндлеру допускаются только авторизованные юзеры.
            Данные анкеты дополняют гео-данные, вместо гео-данных при создании игры учителем, раз мы в нее не игрем.
        """

        db_session = self.request.app['db_session']

        data = await self.request.json()
        region = data.get('region')
        city = data.get('city')
        school = data.get('school')
        grade = data.get('grade')
        # место проживания: сельское/город/заграница (int: 1/2/3)
        residency = data.get('residency')

        if not all((region, city, school, grade, residency)):
            raise HTTPBadRequest(text='Insufficient incoming data.')

        # Заполняя анкету, пользователь уже авторизован через VK, данные из VK занесены в таблицу Users.
        # Данные введенные в анкете заносим в GeoData и ассоциируем с строкой в Users по vk id пользователя.
        user_id = self.request.app.current_user_id
        user = db_session.query(User).filter(User.id == user_id).one_or_none()

        geo_data = GeoData(
            region=region,
            city=city,
            school=school,
            grade=grade,
            residency=residency,
            user=user,
        )

        db_session.add(user, geo_data)
        db_session.commit()
        return json_response({'user_id': user.id})

    async def post(self):
        """Хэндлер для апдейта/указания только имэйла игрока."""
        db_session = self.request.app['db_session']

        data = await self.request.json()
        email = data.get('email')
        if not email:
            raise HTTPBadRequest(text='Insufficient incoming data.')

        user_id = self.request.app.current_user_id
        user = db_session.query(User).filter(User.id == user_id).one_or_none()

        user.email = email

        db_session.add(user)
        db_session.commit()
        return json_response({'user_id': user.id})
