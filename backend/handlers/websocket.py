import logging

import aioredis
from aiohttp import WSMsgType
from aiohttp.web import View, WebSocketResponse

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')


class WebSocketHandlerDebug(View):
    async def get(self) -> WebSocketResponse:
        log.debug(1)
        ws = WebSocketResponse()
        log.debug(2)
        await ws.prepare(self.request)

        async for msg in ws:
            if msg.type == WSMsgType.TEXT:
                if msg.data == 'close':
                    await ws.close()
                else:
                    await ws.send_str(msg.data + '/answer')

        log.debug(3)
        return ws


class WebSocketHandler(View):
    async def get(self) -> WebSocketResponse:
        """
        Создает web socket при обращении от клиента (peer)
        и обрабатывает запросы от клиента в рамках этого сокета.
        try..finally блок для своевременного удаления сессии из объекта приложения перед закрытием сокета (в return).
        Все незакрытые и неудаленные из app сокеты закрываются при завершении прижения в main.py.
        """
        log.debug("In websocket handler")

        player_id = self.request.app.current_user_id
        pin = int(self.request.query.get('pin'))

        pub = await aioredis.create_redis(
            ('redis', 6379),
            encoding='utf-8',
            db=0,
        )

        # Создадим web socket
        ws = WebSocketResponse()
        # Это может быть и учитель, просто ws добавится к учителю а не в лист ws игроков.
        setattr(ws, 'player_id', player_id)
        await ws.prepare(self.request)

        if not self.request.app['gameflow_data'].get(pin):
            await ws.send_str(f'Игровая сессия {pin} отсутствует в кеше.')
            await ws.close()

        game_session_data = self.request.app['gameflow_data'][pin].copy()

        log.debug(f'Websocket connection for {pin} ready')
        if game_session_data['author']['id'] == player_id:
            game_session_data['author']['ws'] = ws

            # для тестов - потом убрать, тестить на реальных клиентских сокетах а не на учительском
            game_session_data['game_data']['total_players'] += 1
            game_session_data['players'].append(ws)

        else:
            total_players = game_session_data['game_data']['total_players']
            max_players = game_session_data['game_data']['max_players']
            if total_players == max_players:
                await ws.send_str('Достигнуто максимальное количество игроков.')
                await ws.close()

            game_session_data['game_data']['total_players'] += 1
            game_session_data['players'].append(ws)

        await ws.send_json({
            'connected': True,
        })

        await pub.publish_json(
            f'session-{pin}',
            {
                'action': 'connected',
                'payload': {
                    'player_id': player_id
                }
            },
        )

        self.request.app['gameflow_data'][pin].update(game_session_data)

        try:
            # Цикл ожидания месседжей от клиента. Мы этим не пользуемся.
            # В основном сокеты только принимают сообщения от redis.
            async for msg in ws:
                log.debug(msg)
                if msg.type == WSMsgType.TEXT:
                    log.debug(msg.data)

                    if msg.data == 'close':
                        await ws.close()
        finally:
            self.request.app['websockets'].discard(ws)
            await pub.quit()

        log.debug('Websocket connection closed.')

        return ws
