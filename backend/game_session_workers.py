import logging
from asyncio import ensure_future, sleep

import aioredis

log = logging.getLogger(__name__)
logging.basicConfig(level='DEBUG')


def do_step_math(game_session_dict, quest_weaknesses, player_id, crisis_id, quest_id):
    """
        Просчет математики по результатам хода игрока (удар по выбранному Quest)
        TODO: Актуализировать математику с уточнением ТЗ в будущем
        TODO: Вероятно надо тут подсчитывать побежден quest или нет, и кризис. Прописывая это в дикт.
    """
    actual_player_skills = game_session_dict['player_stats'][player_id]['actual_skills']
    current_quest_damage = game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints']

    log.debug(f'Actual player skills: {actual_player_skills}')
    log.debug(f'Quest weaknesses (skill ids): {quest_weaknesses}')

    # сопоставим слабости квеста с навыками игрока
    for weakness in quest_weaknesses:
        # ключи дикта при сериализации в JSON контвертнулись из int в str
        weakness = str(weakness)
        # добавим damage'й квесту согласно текущему значению скилла игрока, который релевантен слабости квеста
        current_quest_damage += actual_player_skills[weakness]

        # удар релевантный скиллу игрока
        # прокачаем релевантный скилл игрока на 1 очко
        if actual_player_skills[weakness] > 1:
            log.debug('Hit')
            actual_player_skills[weakness] += 1
        # нерелевантный удар
        # снимем с прокачаных скиллов игрока по 1 очку
        # но прокачаем релевантный скилл игрока на 1 очко
        else:
            log.debug('Miss')
            for skill, value in actual_player_skills.items():
                if value > 1:
                    actual_player_skills[skill] -= 1
            actual_player_skills[weakness] += 1

        # актуализируем глобальный дикт
        game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints'] = current_quest_damage
        game_session_dict['player_stats'][player_id]['actual_skills'] = actual_player_skills

        log.debug(f"Now players skills are: {game_session_dict['player_stats'][player_id]['actual_skills']}")
        log.debug(f"Now quest damage is: {game_session_dict['crisises'][crisis_id]['quests'][quest_id]['hitpoints']}")
        return game_session_dict


async def crisis_timer(this_game):
    timer = 0
    while timer != 25:
        for ws in this_game['players']:
            await ws.send_json({'crisis_timer': timer})
        await sleep(1)
        timer += 1


async def game_session_listener(app, sub, pin):
    """Таск слушающий сообщения для конкретной игровой сессии на канале этой сессии."""

    this_game = app['gameflow_data'][pin]
    res = await sub.subscribe(f'session-{pin}')
    ch = res.pop()

    while await ch.wait_message():
        msg = await ch.get_json()
        log.debug(f'New message in channel {ch.name.decode("ascii")}: {msg}')

        action = msg['action']

        if action == 'connected':
            # Игрок присоединился (успешно открыл сокет). Отправим преподавателю актуальный total players.
            # Этот мессадж в редис приходит из сокета.
            # total players и ws игрока в дикте обновляются в сокете после валидации.
            # Пока игра не начата учителем, игрок видет туториал или крутилку типа "будущее загружается"
            author_ws = this_game['author']['ws']
            await author_ws.send_json(
                {'total_players': this_game['game_data']['total_players']}
            )
            # Игрокам в сокет надо отправить доступных персонажей для их последущего выбора игроком.
            for ws in this_game['players']:
                await ws.send_json(this_game['builds'])

        if action == 'choose_build':
            """"Бесполезный обработчик. Но это пример как рассылать сообщения конкретным игрокам."""
            if not app['gameflow_data'].get(pin):
                for ws in this_game['players']:
                    if ws.player_id == msg['player_id']:
                        await ws.send_json(
                            {
                                'error_message': f'Игровая сессия с PIN: {pin} есть в БД, но отсутствует в кеше.'
                            }
                        )

                        this_game['player_stats'][msg['player_id']] = msg['payload']

            for ws in this_game['players']:
                if ws.player_id == msg['player_id']:
                    await ws.send_json({'success': True})

        if action == 'start_game':
            if not app['gameflow_data'].get(pin):
                for ws in this_game['players']:
                    if ws.player_id == msg['player_id']:
                        await ws.send_json(
                            {
                                'error_message': f'Игровая сессия с PIN: {pin} есть в БД, но отсутствует в кеше.'
                            }
                        )

            # Сосчитаем кол-во hit-point у квестов, обновим игровой кеш
            total_players = this_game['game_data']['total_players']
            quest_hitpoints = total_players // 5

            # Редис конвертит ключи диктов из int в str. Из POST отправлялись int, тут уже получаем str.
            crisises = msg['crisises']

            for c_key, c_val in crisises.items():
                for q_key in c_val['quests'].keys():
                    crisises[c_key]['quests'][q_key]['hitpoints'] = quest_hitpoints

            this_game['crisises'] = crisises

            # Автор игровой сессии иницирует старт игры. Разошлем всем кризисы, просчитав хитпоинты квестов в них.
            for ws in this_game['players']:
                await ws.send_json(crisises)

            ensure_future(crisis_timer(this_game))

        if action == 'make_step':
            if not app['gameflow_data'].get(pin):
                for ws in this_game['players']:
                    if ws.player_id == msg['player_id']:
                        await ws.send_json(
                            {
                                'error_message': f'Игровая сессия с PIN: {pin} есть в БД, но отсутствует в кеше.'
                            }
                        )

            updated_game_data = do_step_math(
                this_game,
                msg['quest_weaknesses'],
                msg['player_id'],
                msg['crisis_id'],
                msg['quest_id'],
            )

            this_game.update(updated_game_data)

        for ws in this_game['players']:
            await ws.send_str(f'New message: {msg}')


async def master_channel_listener(app):
    """Таск слушающий канал master для управления глобальными событиями, н-р создание игровой сессии."""
    sub = await aioredis.create_redis(
        ('redis', 6379),
        encoding='utf-8',
        db=0,
    )
    res = await sub.subscribe('master')
    ch = res.pop()

    while await ch.wait_message():
        msg = await ch.get_json()
        log.debug(f'New message in channel {ch.name.decode("ascii")}: {msg}')
        action = msg['action']

        if action == 'create':
            pin = int(msg['pin'])

            # Дикт для хранения в памяти бекенда временных, изменяемых данных по игровой сессии.
            app['gameflow_data'].update({
                pin: {
                    'players': [],
                    'author': {
                        'ws': None,
                        'id': msg['author_id'],
                        'name': msg['author_name'],
                    },
                    'game_data': {
                        'max_players': msg['max_players'],
                        'total_players': 0,
                        'grade': msg['grade'],
                        'status': 'preparing',
                    },
                    'player_stats': {},
                    'crisises': None,
                    'builds': msg['builds'],
                }
            })

            app['game_session_listeners'].add(ensure_future(game_session_listener(app, sub, pin)))

            log.debug(f'Redis listener for game session: {pin} created.')
