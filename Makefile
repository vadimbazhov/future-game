environment = development

up:
	@make -s check
	@ln -sf conf/$(environment).env .env

	@if [ "$(environment)" = "production" ] ; then\
		docker-compose up --build -d;\
    else \
		docker-compose up --build;\
	fi


check:
	@if [ "$(environment)" = "development" ] && [ ! -f conf/development.env ] ; then\
		echo "File development.env doesn't exist";\
		exit 1;\
	fi

	@if [ "$(environment)" = "production" ] && [ ! -f conf/production.env ] ; then\
		echo "File production.env doesn't exist";\
		exit 1;\
	fi

from_scratch:
	@if [ "$(shell readlink .env)" != "conf/development.env" ]; then\
		echo "This is too dangerous! Do it while on development environment only!";\
		exit 1;\
	fi

	docker-compose down -v
	docker-compose build --pull --no-cache --force-rm
	make -s up
