#!/usr/bin/env bash

set -e

echo "Starting up $APPLICATION of $PROJECT_NAME project in $ENVIRONMENT environment"

echo "Waiting for Postgres"
wait-for-it.sh \
	--host=${POSTGRES_HOST} \
	--port=${POSTGRES_PORT} \
	--timeout=30 \
	--strict \
	-- echo "Postgres is up"

echo "Waiting for Redis"
wait-for-it.sh \
	--host=redis \
	--port=6379 \
	--timeout=30 \
	--strict \
	-- echo "Redis is up"

# Каталог необходим для работы файл-менеджера flask-admin.
[ "$APPLICATION" == "Flask Admin" ] && install -d /app/adm/static

exec $@
