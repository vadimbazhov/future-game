#!/usr/bin/env sh

set -e

echo "Starting Nginx server"

echo "Waiting for HTTP Backend"
wait-for-it.sh \
    --host=http_backend \
    --port=8100 \
    --timeout=60 \
    --strict \
    -- echo "HTTP Backend is up"

while ! test -S /unix_sockets/aiohttp.sock; do
    echo "Waiting for Web-Sockets Backend"
    sleep 1
done

chmod 777 /unix_sockets/ -R
chown :nginx /unix_sockets/ -R

echo "Web-Sockets Backend is up"

exec $@
