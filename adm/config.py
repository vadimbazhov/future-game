import os

DB_USER = os.getenv('POSTGRES_USER', None)
DB_HOST = os.getenv('POSTGRES_HOST', None)
DB_PORT = int(os.getenv('POSTGRES_PORT', None))
DB_PASSWORD = os.getenv('POSTGRES_PASSWORD', None)
DB_NAME = os.getenv('POSTGRES_DB', None)
DB_URI = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'


class Config(object):
    # Flask-admin config
    DEBUG = True
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = '51b64086fae21afcdcddd0c0c4f76471'
    SQLALCHEMY_DATABASE_URI = DB_URI
    UPLOAD_FOLDER = 'static'
    # http://flask-sqlalchemy.pocoo.org/2.3/signals/
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ADMIN_APP_NAME = os.getenv('ADMIN_APP_NAME')

    # Flask-Security config
    SECURITY_PASSWORD_HASH = "pbkdf2_sha512"
    SECURITY_PASSWORD_SALT = "040a05171e271db6ead9eeb05bb9e0a5"

    # Flask-Security URLs, overridden because they don't put a / at the end
    SECURITY_LOGIN_URL = "/login/"
    SECURITY_LOGOUT_URL = "/logout/"

    SECURITY_POST_LOGIN_VIEW = "/admin/"
    SECURITY_POST_LOGOUT_VIEW = "/admin/"

    # Flask-Security features
    SECURITY_REGISTERABLE = False
    SECURITY_SEND_REGISTER_EMAIL = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Некоторые переводы модуля Flask-Security
    SECURITY_MSG_LOGIN = ('Пожалуйста авторизуйтесь, чтобы войти', 'info')
