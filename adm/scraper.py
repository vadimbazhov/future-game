"""
    Из контейнера реез-бекенда вызывать так:
    export PYTHONPATH="/app"; python adm/scraper.py 4
"""

import argparse
import logging
from multiprocessing import Pool

import requests

from adm.database import db_session, City, Region

log = logging.getLogger('Stepika Scraper')

logging.basicConfig(level='DEBUG', format='%(message)s')

cities = dict()
regions = dict()

web_session = requests.Session()

cities_rename_map = {
    'Свердловск': 'Екатеринбург',
}


def get_page_content(url):
    req = web_session.get(
        url,
        headers={
            'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
        },
        timeout=600,
    )
    if req.status_code != 200:
        raise RuntimeError("Page couldn't be fetched.")
    return req.json()


def get_entity(entity):
    for page in range(1, 10_000):
        url = f"https://stepik.org/api/{entity}?page={page}"
        content = get_page_content(url)

        region = None  # for cities

        for el in content[entity]:
            # Если город, и не из России - пропускаем его. У регионов ID страны и города нет, нельзя отфильтровать.
            if entity == 'cities':
                if el['country'] != 2017370:
                    continue

            entity_id = el['id']
            title = el['name_std']
            if entity == 'cities':
                region = el['region']

            # Сбросим переменные на случай пустого массива alt_names в пришедшем со степики элементе
            title_ru = None
            alt_title = None

            for alt_name in el['alt_names']:
                if alt_name.get('language') == 'ru':
                    # города в степике могут иметь неактуальные названия. проверим имя в дикте переименований
                    title_ru = cities_rename_map.get(alt_name['name'], alt_name['name'])
                if alt_name.get('language') == 'en':
                    alt_title = alt_name['name']

            if entity == 'cities':
                cities.update({
                    entity_id: {
                        'title': title,
                        'title_ru': title_ru,
                        'alt_title': alt_title,
                        'region': region,
                    }
                })
                log.debug(f'Len of Cities: {len(cities)}')

            if entity == 'regions':
                regions.update({
                    entity_id: {
                        'title': title,
                        'title_ru': title_ru,
                        'alt_title': alt_title,
                    }
                })
                log.debug(f'Len of Regions: {len(regions)}')

        if not content['meta']['has_next']:
            log.debug(f'Pages for entity: {entity} are all scraped.')
            break

    if entity == 'cities':
        return entity, cities
    if entity == 'regions':
        return entity, regions


def fill_db(entity, data):
    if entity == 'regions':
        for region_id, region in data.items():
            db_session.add(
                Region(id=region_id, title=region['title'], title_ru=region['title_ru'],
                       alt_title=region['alt_title']
                       )
            )
        db_session.commit()
        db_session.execute("ALTER SEQUENCE region_id_seq RESTART WITH 1;")
        log.debug(f'{entity.capitalize()} added: {len(data)}')

    if entity == 'cities':
        for city_id, city in data.items():
            db_session.add(
                City(id=city_id, title=city['title'], title_ru=city['title_ru'], alt_title=city['alt_title'],
                     region_id=city['region'],
                     )
            )
        db_session.commit()
        db_session.execute("ALTER SEQUENCE city_id_seq RESTART WITH 1;")
        log.debug(f'{entity.capitalize()} added: {len(data)}')

    return


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("jobs", help="Number of simultaneous jobs.", type=int)
    jobs = parser.parse_args().jobs

    pool = Pool(jobs)

    # порядок важен. города заносим в конце т.к. у гродов в БД есть FK на регионы, которые должны быть уже наполнены.
    entities = ('regions', 'cities')

    # map сохраняет порядок отдачи результатов согласно порядку элементов во входящем итераторе
    for entity, data in pool.map(get_entity, entities):
        fill_db(entity, data)


if __name__ == '__main__':
    main()
