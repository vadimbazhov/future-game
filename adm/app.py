import os

from flask import Flask, request, redirect, url_for, abort
from flask_admin import Admin as Flaskadmin, AdminIndexView, BaseView, expose, helpers
from flask_admin.contrib.fileadmin import FileAdmin
from flask_admin.contrib.sqla import ModelView
from flask_security import Security, SQLAlchemyUserDatastore, UserMixin, RoleMixin, login_required, current_user, \
    logout_user
from flask_sqlalchemy import SQLAlchemy

from adm.database import initdb, Build, Crisis, Skill, User, Quest, GameSession, City, Region, UserSession, Step, \
    GeoData

app = Flask(__name__)
app.config.from_object('adm.config.Config')
db = SQLAlchemy(app)


# Модели User и Role определяются тут только для flask-security. Сами таблицы и их содержимое создаются через ORM.
class Admin(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary='roles_admins',
                            backref=db.backref('admins', lazy='dynamic'))

    def __str__(self):
        return self.email


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name


roles_admins = db.Table(
    'roles_admins',
    db.Column('admin_id', db.Integer(), db.ForeignKey('admin.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)

user_datastore = SQLAlchemyUserDatastore(db, Admin, Role)
security = Security(app, user_datastore)


class SecureMixin:
    @staticmethod
    def is_accessible():
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.has_role('superuser'):
            return True
        return False

    def _handle_view(self, name, **kwargs):
        """
        Override builtin _handle_view in order to redirect users when a view is not accessible.
        """
        if not self.is_accessible():
            if current_user.is_authenticated:
                abort(403)
            else:
                return redirect(url_for('security.login', next=request.url))


class SecureModelView(SecureMixin, ModelView):
    pass


class BuildAdmin(SecureModelView):
    pass


class QuestAdmin(SecureModelView):
    pass


class SkillAdmin(SecureModelView):
    pass


class CrisisAdmin(SecureModelView):
    pass


class UserAdmin(SecureModelView):
    form_excluded_columns = ['registered_at']
    form_columns = ('id', 'first_name', 'last_name', 'birth_date', 'email', 'photo',
                    'access_token', 'game_sessions', 'created_sessions', 'geo_data')
    column_descriptions = {'id': 'Цифровой идентификатор пользователя в ВК, пример 33632451'}

    def get_edit_form(self):
        form = self.scaffold_form()
        delattr(form, 'id')
        return form


class GameSessionAdmin(SecureModelView):
    """
    Настройками в этой вьюхе управляем поведением работы с primary key. Проблемы связаные с этим и их решения тут:
    https://github.com/flask-admin/flask-admin/issues/52
    """
    form_excluded_columns = ['created_at']
    column_display_pk = True
    form_columns = ('id', 'max_players', 'author', 'player', 'geo_data')
    column_descriptions = {'id': "6-значное рандомное число"}

    def get_edit_form(self):
        form = self.scaffold_form()
        delattr(form, 'id')
        return form


class GeoDataAdmin(SecureModelView):
    form_excluded_columns = ['created_at']
    column_display_pk = True


class UserSessionAdmin(SecureModelView):
    column_searchable_list = ['id']
    column_display_pk = True


class StepAdmin(SecureModelView):
    pass


class MyFileAdmin(SecureMixin, FileAdmin):
    pass


class CityAdmin(SecureModelView):
    # column_display_pk = True
    column_searchable_list = ['title', 'title_ru', 'alt_title']
    pass


class RegionAdmin(SecureModelView):
    # column_display_pk = True
    column_searchable_list = ['title', 'title_ru', 'alt_title']
    pass


# Define a context processor for merging flask-admin's template context into the flask-security views.
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=helpers,
        get_url=url_for
    )


# Flask views
@login_required
@app.route('/')
def index():
    return redirect('/admin', code=302)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')


class LogoutView(BaseView):
    @expose('/')
    def index(self):
        logout_user()
        return redirect('/')


admin = Flaskadmin(app, name=app.config['ADMIN_APP_NAME'], template_mode='bootstrap3', index_view=AdminIndexView())

# Дефолтные настройки файл-менеджера достаточно permissive, вероятно надо захарденить:
# http://flask-admin.readthedocs.io/en/latest/api/mod_contrib_fileadmin/
static_path = os.path.join(os.path.dirname(__file__), 'static')

initdb()

# Add views
admin.add_views(
    BuildAdmin(Build, db.session),
    QuestAdmin(Quest, db.session),
    SkillAdmin(Skill, db.session),
    CrisisAdmin(Crisis, db.session),
    UserAdmin(User, db.session),
    GameSessionAdmin(GameSession, db.session),
    UserSessionAdmin(UserSession, db.session),
    StepAdmin(Step, db.session),

    CityAdmin(City, db.session),
    RegionAdmin(Region, db.session),
    GeoDataAdmin(GeoData, db.session),

    MyFileAdmin(static_path, '/static/', name='Static Files'),
    LogoutView(name='Logout'),
)
