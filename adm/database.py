import os
from datetime import datetime

from sqlalchemy import create_engine, Column, DateTime, ForeignKey, Integer, String, Table, Boolean, Unicode, \
    UnicodeText, SmallInteger, inspect
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from adm.config import DB_URI

Base = declarative_base()
metadata = Base.metadata

db = create_engine(DB_URI)
Session = sessionmaker(bind=db)
db_session = Session()

ADMIN_LOGIN = os.getenv('ADMIN_APP_LOGIN')
ADMIN_SECRET = os.getenv('ADMIN_APP_PASS')


class Build(Base):
    __tablename__ = 'build'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(60), unique=True)
    image = Column(String(100), unique=True)
    description = Column(UnicodeText, unique=True)
    skills = relationship('Skill', secondary='build_skill')

    def __repr__(self):
        return f'{self.title}'


class Skill(Base):
    __tablename__ = 'skill'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(60), unique=True)

    def __repr__(self):
        return f'{self.title}'


class Crisis(Base):
    __tablename__ = 'crisis'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(60), unique=True)
    image = Column(String(100), unique=True)
    description = Column(UnicodeText, unique=True)
    success_text = Column(UnicodeText, unique=True)
    fail_text = Column(UnicodeText, unique=True)
    quest = relationship("Quest", back_populates="crisis")

    def __repr__(self):
        return f'{self.title}'


class Quest(Base):
    __tablename__ = 'quest'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(60), unique=True)
    image = Column(String(100), unique=True)
    description = Column(UnicodeText, unique=True)
    success_text = Column(UnicodeText, unique=True)
    fail_text = Column(UnicodeText, unique=True)

    crisis_id = Column(Integer, ForeignKey('crisis.id'))
    crisis = relationship('Crisis', back_populates="quest")
    weaknesses = relationship('Skill', secondary='quest_skill')

    def __repr__(self):
        return f'{self.title}'


class GameSession(Base):
    __tablename__ = 'game_session'

    id = Column(Integer, primary_key=True)
    # 6-циферный рандомный код. Очищается у записей чей created_at старее суток.
    # TODO: Очищать раз в сутки crontab'ом или еще как то, например asyncio таском.
    pin = Column(Integer)
    max_players = Column(SmallInteger, nullable=False)
    created_at = Column(DateTime, default=datetime.utcnow)

    player = relationship('User', secondary='game_session_player', back_populates="game_sessions")

    author_id = Column(Integer, ForeignKey('user.id'))
    author = relationship('User', back_populates="created_sessions")

    geo_data = relationship("GeoData", uselist=False, back_populates="game_session")

    def __repr__(self):
        return f'PIN,GameSession:{self.pin},{self.id} - {self.geo_data}'


class User(Base):
    __tablename__ = 'user'

    # Данные из VK, апдейтящиеся после каждой авторизации через VK
    id = Column(Integer, primary_key=True, autoincrement=False)  # VK ID
    first_name = Column(Unicode(80), nullable=False)
    last_name = Column(Unicode(80), nullable=False)
    birth_date = Column(String(60), nullable=False)
    email = Column(String(150), nullable=False, unique=True)
    photo = Column(Unicode(), nullable=False)
    access_token = Column(Unicode(100), nullable=False)

    registered_at = Column(DateTime, default=datetime.utcnow)

    game_sessions = relationship(
        "GameSession",
        secondary='game_session_player',
        back_populates="player")
    created_sessions = relationship("GameSession", back_populates="author")

    geo_data = relationship("GeoData", uselist=False, back_populates="user")

    def __repr__(self):
        return f'{self.email}'


class GeoData(Base):
    __tablename__ = 'geo_data'
    id = Column(Integer, primary_key=True)
    region = Column(Unicode(80), nullable=False)
    city = Column(Unicode(60), nullable=False)
    school = Column(Unicode(150), nullable=False)
    grade = Column(Unicode(5), nullable=False)
    # место проживания: сельское/город/заграница (int: 1/2/3)
    residency = Column(SmallInteger, nullable=False)

    # Необязательные поля, которые будут заполняться в будущем
    # Max Varnavskiy: сейчас это мы не будем добавлять в анкеты, но к командному этапу пригодятся
    tutoring_company = Column(Unicode())
    tutor_fio = Column(Unicode(150))
    tutor_email = Column(String(150))

    created_at = Column(DateTime, default=datetime.utcnow)

    game_session = relationship("GameSession", back_populates="geo_data")
    game_session_id = Column(Integer, ForeignKey('game_session.id'))
    user = relationship("User", back_populates="geo_data")
    user_id = Column(Integer, ForeignKey('user.id'))

    def __repr__(self):
        return f'{self.city}, {self.school}, {self.grade}'


t_quest_skill = Table(
    'quest_skill', metadata,
    Column('quest_id', ForeignKey('quest.id')),
    Column('skill_id', ForeignKey('skill.id'))
)

t_build_skill = Table(
    'build_skill', metadata,
    Column('build_id', ForeignKey('build.id')),
    Column('skill_id', ForeignKey('skill.id'))
)

t_game_session_player = Table(
    'game_session_player', metadata,
    Column('game_session_id', ForeignKey('game_session.id')),
    Column('player_id', ForeignKey('user.id'))
)


# User and Role tables for flask-security
class Admin(Base):
    __tablename__ = 'admin'

    id = Column(Integer, primary_key=True)
    email = Column(String(255), unique=True)
    password = Column(String(255))
    active = Column(Boolean)
    confirmed_at = Column(DateTime)


class Role(Base):
    __tablename__ = 'role'

    id = Column(Integer, primary_key=True)
    name = Column(String(80), unique=True)
    description = Column(String(255))

    admins = relationship('Admin', secondary='roles_admins')


t_roles_admins = Table(
    'roles_admins', metadata,
    Column('admin_id', ForeignKey('admin.id')),
    Column('role_id', ForeignKey('role.id'))
)


class City(Base):
    __tablename__ = 'city'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(100))
    title_ru = Column(Unicode(100))
    alt_title = Column(String(100))

    region_id = Column(Integer, ForeignKey('region.id'))
    region = relationship('Region', back_populates="cities")

    def __repr__(self):
        return f'{self.title}'


class Region(Base):
    __tablename__ = 'region'

    id = Column(Integer, primary_key=True)
    title = Column(Unicode(100))
    title_ru = Column(Unicode(100))
    alt_title = Column(String(100))

    cities = relationship('City', back_populates="region")

    def __repr__(self):
        return f'{self.title}'


class UserSession(Base):
    """
        Таблица-лог для хранения участвующих в играх игроков с их начальными характеристиками (build),
        которые они выбрали при старте игры.
     """
    __tablename__ = 'user_session'

    id = Column(Integer, primary_key=True)

    game_session_id = Column(Integer, ForeignKey('game_session.id'))
    game_session = relationship('GameSession')

    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship('User')

    build_id = Column(Integer, ForeignKey('build.id'))
    build = relationship('Build')

    def __repr__(self):
        return f'{self.id}'


class Step(Base):
    """
        Таблица-лог для хранения действий игроков в игровых сессиях.
        В какой игре, какой пользователь, бил какого босса решая какой кризис.
    """
    __tablename__ = 'step'

    id = Column(Integer, primary_key=True)

    user_session_id = Column(Integer, ForeignKey('user_session.id'))
    user_session = relationship('UserSession')

    crisis_id = Column(Integer, ForeignKey('crisis.id'))
    crisis = relationship('Crisis')

    quest_id = Column(Integer, ForeignKey('quest.id'))
    quest = relationship('Quest')


def initdb():
    inspector = inspect(db)
    if inspector.get_table_names():
        return

    Base.metadata.drop_all(db)
    Base.metadata.create_all(db)

    admin = Admin(email=ADMIN_LOGIN, password=ADMIN_SECRET, active=True, confirmed_at=datetime.utcnow())
    role = Role(name='superuser', description='administrative role', admins=[admin])

    sk1 = Skill(title='Ум')
    sk2 = Skill(title='Сила')
    sk3 = Skill(title='Выносливость')
    b1 = Build(title='Тактик', image='tactic.png', description='Умный и сильный',
               skills=[sk1, sk2])
    q1 = Quest(title='Толстый тролль', weaknesses=[sk1, sk2], description='Огромный вонючий но медленный троль',
               image='troll.img')
    q2 = Quest(title='Старый демон', weaknesses=[sk3, sk2], description='Старый ворчливый сатир', image='oldsatir.img')

    db_session.add_all([
        admin,
        role,
        sk1,
        sk2,
        sk3,
        b1,
        q1,
        q2,
        Crisis(title='Рагнарёк', image='ragnarok.img', description='Небеса разверзнутся и боги потопят землю !',
               quest=[q1, q2]),
        Crisis(title='Судный день', image='doomsday.img',
               description='Второе пришествие настало и вы поплатитесь за свои грехи !'),
        Crisis(title='Апокалипсис', image='apocalypse.img',
               description='Луна падает на землю а солнце так близко что сжигает поверхность планеты !'),
    ])

    db_session.commit()
