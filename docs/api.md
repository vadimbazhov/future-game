# API

# Хэндлеры непосредственно игрового процесса
`pin`: 6-значный int 

## Создание игровой сесии учителем

    POST: /game_session
    BODY: {
            "region":"Свердловская область",
            "city":"Екатеринбург",
            "school":"Школа № 17",
            "grade": "10б",
            "residency": <int: [1,2,3]>,
            "max_players": 30,
          }
    RESP: {
            "game_session_pin": <int>,
            "qr_code_url": <url: str>,
            "join_game_url": <url: str>,
            "ws_url": <url: str>,
          }

## Подключение игрока к игровой сесии

    WS: ws://<domain>/ws?pin=<pin: int>

## Выбор характеристики, билда игроком перед началом игры:

    POST: /choose_build
    BODY: {"build_id":1, "pin": <pin: int>}
    RESP: {"success": true}

## Старт игры.
Инициируется преподавателем. Игрокам рассылаются кризисы с квестами с посчитанными хинтпоинтами.

    POST: /start_game
    BODY: {"pin": <pin: int>}
    RESP: {"success": true}

## Ход. Выбор Квеста игроком.

    POST: /make_step
    BODY: {
            "pin": <pin: int>,
            "crisis_id": <int>,
            "quest_id": <int>,
          }
    RESP: {"success": true}

# Остальные хэндлеры

## Авторизация через ВК.
Клиентская часть должна поддерживать редиректы.

    GET: /auth
    RESP: {"authorized": true}

## Запрос игровой сессии
Выдает url на вебсокет игровой сессии и pin на всякий случай.

    GET: /game_session?pin=<pin: int>
    RESP: {
            "game_session_link": <ws url: str>,
            "game_session_pin": <pin: int>,
          }

## Получить QR-код в виде картинки.
С зашитой в него ссылкой на вебсокет конкретной игровой сессии. Переход по ней коннектит ученика в игру.

    GET: /game_session_qr?pin=<pin: int>
    RESP: <image: bytes>

## Поиск страны, региона, города.
При поиске города опционально можно передать ID страны и региона полученные на предыдущих шагах заполнения формы.
`query`: строка любого регистра, латиницей или кириллицей, минимум 3 символа

    GET: /geo_search?regions=<query: str>
    GET: /geo_search?cities=<query: str>&region_id=<int>
    
    RESP: {
            "regions": [],
            "cities": [
              [
                <title: str>,
                <id: int>,
              ],
            ]
          }

## Обновление/задание почты у ученика.
На этот хэндлер должен происходить редирект перед регистрацией на степике если имэйл у ученика отсутствует (не был задан в ВК).

    POST: /user_email
    BODY: {"email":"asd@wer.ru"}
    RESP: {"user_id": <vk id: int>}

## Обновление польз. данных в БД или создание их с нуля.
Пока что вызывается в случае если ученик не хочет играть но на степике его надо регистрировать.

    PATCH: /user_info
    BODY: {"region":"Костромская область", "city":"Кострома", "school":"Школа № 224", "grade": "11б", "residency": 1, "email": "patched@ema.il"}
    RESP: {"user_id": <vk id: int>}
